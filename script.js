let firstName = document.querySelector("#txt-first-name");
let lastName = document.querySelector("#txt-last-name");

let fullName = document.getElementById("span-full-name");

const updateName = () => {
	let txtFirst = firstName.value;
	let txtLast = lastName.value;

	fullName.innerHTML = `${txtFirst} ${txtLast}`
}

firstName.addEventListener("keyup", updateName);
lastName.addEventListener("keyup", updateName);
